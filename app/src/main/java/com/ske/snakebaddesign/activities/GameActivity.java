package com.ske.snakebaddesign.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ske.snakebaddesign.R;
import com.ske.snakebaddesign.guis.BoardView;
import com.ske.snakebaddesign.model.Board;
import com.ske.snakebaddesign.model.Die;
import com.ske.snakebaddesign.model.Game;
import com.ske.snakebaddesign.model.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class GameActivity extends AppCompatActivity implements Observer{

    private Game game;
    private BoardView boardView;
    private Button buttonTakeTurn;
    private Button buttonRestart;
    private TextView textPlayerTurn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        resetGame();
    }

    private void initComponents() {
        boardView = (BoardView) findViewById(R.id.board_view);
        buttonTakeTurn = (Button) findViewById(R.id.button_take_turn);
        buttonTakeTurn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeTurn();
            }
        });
        buttonRestart = (Button) findViewById(R.id.button_restart);
        buttonRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boardView.setBoard(new Board(4));
                resetGame();
            }
        });
        textPlayerTurn = (TextView) findViewById(R.id.text_player_turn);
        game = new Game(boardView.getBoard().getBoardSize());
        game.addObserver(this);
        boardView.setBoardSize(game.getBoardSize());
    }

    private void resetGame() {
        game.resetGame();
        refresh();
    }

    private void refresh(){
        boardView.setP1Position(game.getPlayer1().getPice().getPosition());
        boardView.setP2Position(game.getPlayer2().getPice().getPosition());
        if (game.getTurn() % 2 == 0) {
            textPlayerTurn.setText("Player 1's Turn");
        } else {
            textPlayerTurn.setText("Player 2's Turn");
        }
    }

    private void takeTurn() {
        int value = game.rollDie();
        String title = "You rolled a die";
        String msg = "You got " + value;
        OnClickListener listener = new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                refresh();
                dialog.dismiss();
            }
        };

        displayDialog(title, msg, listener);

    }

    private void displayDialog(String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", listener);
        alertDialog.show();
    }

    private void showDialogWin(String string){
        String title = "Game Over";
        String msg = string;
        OnClickListener listener = new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                resetGame();
                dialog.dismiss();

            }
        };
        displayDialog(title, msg, listener);
    }

    public void update(Observable o,Object obj){

        if(game.checkWin() == game.getPlayer1()){
            showDialogWin("Player 1 won!");
        }else if(game.checkWin() == game.getPlayer2()){
            showDialogWin("Player 2 won!");
        }
    }

}
