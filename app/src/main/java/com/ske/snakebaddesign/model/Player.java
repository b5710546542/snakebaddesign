package com.ske.snakebaddesign.model;

/**
 * Created by Noot on 16/3/2559.
 */
public class Player {

    private Piece pice;

    public Player(){
        pice = new Piece();
    }

    public Piece getPice() {
        return pice;
    }

    public void setPiece(int newPos) {
        pice.setPosition(newPos);
    }

}
