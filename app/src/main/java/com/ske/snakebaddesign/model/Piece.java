package com.ske.snakebaddesign.model;

/**
 * Created by Noot on 16/3/2559.
 */
public class Piece {

    private int position;

    public Piece(){
        this.position = 0;
    }

    public int getPosition(){
        return position;
    }

    public void setPosition(int position){
        this.position = position;
    }

}
