package com.ske.snakebaddesign.model;

import java.util.Random;

/**
 * Created by Noot on 16/3/2559.
 */
public class Die {

    private int point;

    public Die(){
        this.point = 0;
    }
    public int roll(){
        return 1 + new Random().nextInt(6);
    }
}
