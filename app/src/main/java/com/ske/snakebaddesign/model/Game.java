package com.ske.snakebaddesign.model;


import java.util.Observable;

/**
 * Created by Noot on 16/3/2559.
 */
public class Game extends Observable{

    private Player player1;
    private Player player2;
    private Die die;
    private int boardSize;
    private int turn;

    public Game(int boardSize){
        player1 = new Player();
        player2 = new Player();
        die = new Die();
        this.boardSize = boardSize;
    }

    public int rollDie(){
        final int value = die.roll();
        moveCurrentPiece(value);
        return value;
    }

    public void resetGame(){
        player1.setPiece(0);
        player2.setPiece(0);
        turn = 0;
    }

    private void moveCurrentPiece(int value) {

        if (turn % 2 == 0) {
            player1.setPiece(adjustPosition(player1.getPice().getPosition(), value));
            this.setChanged();
            this.notifyObservers(player1);
        } else {
            player2.setPiece(adjustPosition(player2.getPice().getPosition(), value));
            this.setChanged();
            this.notifyObservers(player2);
        }
//        checkWin();
        turn++;
    }

    private int adjustPosition(int current, int distance) {
        current = current + distance;
        int maxSquare = boardSize * boardSize - 1;
        if(current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        return current;
    }

    public Player checkWin() {

        if (player1.getPice().getPosition() == boardSize * boardSize - 1) {
            return player1;
        } else if (player2.getPice().getPosition() == boardSize * boardSize - 1) {
            return player2;
        } else {
            return null;
        }
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public int getBoardSize(){
        return boardSize;
    }

    public Die getDie() {
        return die;
    }

    public int getTurn() {
        return turn;
    }


}
