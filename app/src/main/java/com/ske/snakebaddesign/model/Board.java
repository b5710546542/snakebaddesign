package com.ske.snakebaddesign.model;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Noot on 16/3/2559.
 */
public class Board{

    private int boardSize;
    private List<Square> squareList;

    public Board(int boardSize){
        this.boardSize = boardSize;
        squareList = new ArrayList<Square>();
        resetColor();
    }

    public void resetColor(){
        for(int i = 0 ; i < boardSize*boardSize ; i++){
            squareList.add(new Square(Color.parseColor("#" + (1 + new Random().nextInt(9)) + "" + ((1 + new Random().nextInt(9))) + "aa" + (1 + new Random().nextInt(9)) +"c")));
        }

    }

    public int getBoardSize(){
        return boardSize;
    }

    public List<Square> getSquareList(){
        return squareList;
    }

}
