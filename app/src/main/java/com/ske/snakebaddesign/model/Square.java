package com.ske.snakebaddesign.model;

/**
 * Created by Noot on 16/3/2559.
 */
public class Square {

    private int color;

    public Square(int color){
        this.color = color;
    }

    public int getColor(){
        return color;
    }

    public void setColor(int color){
        this.color = color;
    }
}
