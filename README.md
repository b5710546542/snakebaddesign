# README #

### Domain model ###

![DMD.jpeg](https://bitbucket.org/repo/odrzKr/images/1257797805-DMD.jpeg)

### What's about each class ###

Board ( it is controller of BoardView )

* resetColor : set color of board from square

Die

* roll : random the value from 1-6

Game ( it is controller of GameActivity )

* rollDie : get random value from die and move piece
* resetGame : to set beginning of game
* moveCurrentPiece : set current position of piece
* adjustPosition : to check position with current and distance
* checkWin

Piece

- keep position of player

Player

- each player have one piece 

Square

- keep value of color

BoardView

- BoardView contain Board

GameActivity

- GameActivity contain Game

### New feature ###

* When reset game the color of board will be reset by random